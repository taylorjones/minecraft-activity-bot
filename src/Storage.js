const items = [];

class Storage {
  getItem(name) {
    return items[name];
  }

  setItem(name, value) {
    items[name] = value;
  }

  init() {
    // Implementing API for tests
  }
}

module.exports = Storage;