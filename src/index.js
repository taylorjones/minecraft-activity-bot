#!/usr/bin/env node
require('dotenv').config();
const storage = require('node-persist');
const MinecraftBot = require('./MinecraftBot');
const { Client, Intents } = require('discord.js');

process.env.UV_THREADPOOL_SIZE = 128;
// process.env.NODE_DEBUG = 'dns';
process.on('uncaughtException', (err) => console.error(`Uncaught exception: ${err}\n${err.stack}`));

async function main() {
  const client = new Client({ intents: [ Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES ] });
  const minecraftBot = new MinecraftBot(storage, client);
  const startup = Promise.all([
    new Promise((resolve) => client.on('ready', resolve)).then(() => {
      client.application.commands.set([
        {
          name: 'ping',
          description: 'Replies with Pong!',
        },
      ].concat(minecraftBot.getCommands()));
    }),
    storage.init().then(minecraftBot.loadCache),
  ]).then(() => console.log('Ready!'));

  client.on('interactionCreate', async interaction => {
    if (interaction.isCommand()) {

      if (interaction.commandName === 'ping') {
        await interaction.reply('Pong!');
      }
      else {
        minecraftBot.handleCommand(interaction);
      }
    }
    else if (interaction.isAutocomplete()) {
      await minecraftBot.handleAutocomplete(interaction);
    }
  });

  client.login(process.env.DISCORD_TOKEN);

  startup.then(() => {
    minecraftBot.updateStatus();
    setInterval(minecraftBot.updateStatus, 60000);
    client.user.setPresence({
      status: 'online',
      afk: false,
      activities: [{
        name: 'Minecraft | mb/help',
        type: 'WATCHING',
      }],
    });
    process.on('SIGINT', function() {
      console.log('About to exit.');
      client.user.setPresence({
        status: 'invisible',
        afk: false,
        activities: [],
      });
      console.log('All cleaned up');
      process.exit(0);
    });
  });

  return startup;
}

main();
