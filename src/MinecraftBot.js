const dns = require('native-dns');
const Gamedig = require('gamedig');
const ampapi = require('@cubecoders/ampapi');
const { ApplicationCommandOptionType, ApplicationCommandType, ChannelType } = require('discord-api-types/v9');

const groups = [];
const serverCache = new Map();
const ampCache = new Map();

let m_storage;
let m_client;
let _this;

class MinecraftBot {

  constructor(storage, client) {
    _this = this;
    m_storage = storage;
    m_client = client;
  }

  save() {
    m_storage.setItem('groups', groups);
  }

  getCommands() {
    return [
      {
        name: 'addserver',
        description: 'Add a server to monitor',
        type: ApplicationCommandType.ChatInput,
        options: [
          {
            name: 'host',
            description: 'The host to monitor: hostname, hostname:port or ip:port',
            required: true,
            type: ApplicationCommandOptionType.String,
          },
          {
            name: 'alias',
            description: 'The alias to refer to to this server by',
            required: true,
            type: ApplicationCommandOptionType.String,
          },
        ],
      },
      {
        name: 'delserver',
        description: 'Remove a server from monitoring',
        type: ApplicationCommandType.ChatInput,
        options: [
          {
            name: 'alias',
            description: 'The alias that was set for this server',
            required: true,
            type: ApplicationCommandOptionType.String,
          },
        ],
      },
      {
        name: 'setchannel',
        description: 'Tell Minecraft Bot to send messages to this channel',
        type: ApplicationCommandType.ChatInput,
        options: [
          {
            name: 'channel',
            description: 'The channel to send status messages to',
            type: ApplicationCommandOptionType.Channel,
            channelTypes: [ChannelType.GuildText],

          },
        ],
      },
      {
        name: 'list',
        description: 'Display which servers Minecraft Bot is monitoring',
        type: ApplicationCommandType.ChatInput,
      },
      {
        name: 'amp',
        description: 'Management commands for amp',
        type: ApplicationCommandType.ChatInput,
        options: [
          {
            name: 'login',
            description: 'Login to amp. This only needs done once.',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'hostname',
                description: 'Hostname to the amp server',
                type: ApplicationCommandOptionType.String,
              },
              {
                name: 'username',
                description: 'Username to log into amp',
                type: ApplicationCommandOptionType.String,
              },
              {
                name: 'password',
                description: 'Password to login to amp',
                type: ApplicationCommandOptionType.String,
              },
            ],
          },
          {
            name: 'instances',
            description: 'Get current status of all instances',
            type: ApplicationCommandOptionType.Subcommand,
          },
          {
            name: 'startinstance',
            description: 'Start the amp instance',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'instancename',
                description: 'Instance to act on',
                type: ApplicationCommandOptionType.String,
                autocomplete: true,
              },
            ],
          },
          {
            name: 'stopinstance',
            description: 'Stop the amp instance',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'instancename',
                description: 'Instance to act on',
                type: ApplicationCommandOptionType.String,
                autocomplete: true,
              },
            ],
          },
          {
            name: 'info',
            description: 'Get current status of game server inside instance',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'instancename',
                description: 'Instance to act on',
                type: ApplicationCommandOptionType.String,
                autocomplete: true,
              },
            ],
          },
          {
            name: 'start',
            description: 'Start game server inside instance',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'instancename',
                description: 'Instance to act on',
                type: ApplicationCommandOptionType.String,
                autocomplete: true,
              },
            ],
          },
          {
            name: 'stop',
            description: 'Stop game server inside instance',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'instancename',
                description: 'Instance to act on',
                type: ApplicationCommandOptionType.String,
                autocomplete: true,
              },
            ],
          },
          {
            name: 'update',
            description: 'Update game server inside instance',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'instancename',
                description: 'Instance to act on',
                type: ApplicationCommandOptionType.String,
                autocomplete: true,
              },
            ],
          },
          {
            name: 'whitelist',
            description: 'Add user to whitelist on instance',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'instancename',
                description: 'Instance to act on',
                type: ApplicationCommandOptionType.String,
                autocomplete: true,
              },
              {
                name: 'user',
                description: 'User to whitelist',
                type: ApplicationCommandOptionType.String,
              },
            ],
          },
          {
            name: 'dewhitelist',
            description: 'Remove user form whitelist on instance',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'instancename',
                description: 'Instance to act on',
                type: ApplicationCommandOptionType.String,
                autocomplete: true,
              },
              {
                name: 'user',
                description: 'User to remove from whitelist',
                type: ApplicationCommandOptionType.String,
              },
            ],
          },
          {
            name: 'listwhitelist',
            description: 'List users whitelisted on instance',
            type: ApplicationCommandOptionType.Subcommand,
            options: [
              {
                name: 'instancename',
                description: 'Instance to act on',
                type: ApplicationCommandOptionType.String,
                autocomplete: true,
              },
            ],
          },
        ],
      },
    ];
  }

  async _getGroup(guildId) {
    let group = groups.find((e) => (e.id == guildId));
    if (group === undefined) {
      group = {
        id: guildId,
        servers: [],
        ampserver: undefined,
        channel: undefined,
      };
      groups.push(group);
    }
    return group;
  }

  async handleCommand(interaction) {
    try {
      const group = await _this._getGroup(interaction.guildId);

      switch (group, interaction.commandName) {
      case 'addserver':
        return await _this._addServer(group, interaction);
      case 'delserver':
        return await _this._deleteServer(group, interaction);
      case 'setchannel':
        return await _this._setChannel(group, interaction);
      case 'list':
        return await _this._listServers(group, interaction);
      case 'amp':
        return await _this._handleAmpCommand(group, interaction);
      default:
        return interaction.reply('Unsupported command.');
      }
    }
    catch (error) {
      interaction.reply(`Error while processing command ${error}`);
      console.log(error);
    }
  }

  async _handleAmpCommand(group, interaction) {
    const subcommand = interaction.options.getSubcommand();
    switch (subcommand) {
    case 'login':
      return await _this._ampLogin(group, interaction);
    case 'startinstance':
      return await _this._ampStartInstance(group, interaction);
    case 'stopinstance':
      return await _this._ampStopInstance(group, interaction);
    case 'info':
      return await _this._ampInfo(group, interaction);
    case 'instances':
      return await _this._ampGetInstances(group, interaction);
    case 'startgame':
      return await _this._ampStartGame(group, interaction);
    case 'stopgame':
      return await _this._ampStopGame(group, interaction);
    case 'update':
      return await _this._ampUpdateGame(group, interaction);
    case 'whitelist':
      return await _this._ampWhitelist(group, interaction);
    case 'dewhitelist':
      return await _this._ampDeWhitelist(group, interaction);
    case 'listwhitelist':
      return await _this._ampListWhitelist(group, interaction);
    default:
      return await interaction.reply('Unsupported command.');
    }
  }

  async handleAutocomplete(interaction) {
    const group = await _this._getGroup(interaction.guildId);
    const focusedOption = interaction.options.getFocused(true);
    switch (focusedOption.name) {
    case 'instancename':
      await _this._autocompleteInstanceNames(group, interaction, focusedOption.value);
      break;
    }
  }

  async _autocompleteInstanceNames(group, interaction, userInput) {
    if (!ampCache.has(group.id)) {
      return interaction.respond([]);
    }
    const API = ampCache.get(group.id).get('main');
    const instances = await _this._ampGetInstancesAsync(API);
    const choices = instances.reduce((previous, current) => previous.concat(current.AvailableInstances), [])
      .filter((instance) => !userInput || instance.InstanceName.toLowerCase().startsWith(userInput.toLowerCase()) || instance.FriendlyName.toLowerCase().startsWith(userInput.toLowerCase()))
      .map(instance => ({ 'name': instance.FriendlyName, 'value': instance.InstanceName }));
    return interaction.respond(choices);
  }

  _addServer(group, interaction) {
    return new Promise(resolve => {
      const options = interaction.options;
      const host = options.getString('host', true);
      const alias = options.getString('alias', true);

      const server = group.servers.find(s => s.host == host || s.alias == alias);
      if (server !== undefined) {
        interaction.reply(`Server with host or alias already exists\nServer: ${server.alias} - \`${server.host}\``);
      }
      const parts = host.split(':');

      const pinghost = parts[0];
      if (parts.length == 1) {
        dns.resolveSrv(`_minecraft._tcp.${pinghost}`, (err, answer) => {
          if (answer != undefined && answer.length > 0) {
            _this._saveServer(interaction, group, host, alias, answer[0].name, answer[0].port);
            resolve();
          }
          else {
            _this._saveServer(interaction, group, host, alias, pinghost, 25565);
            resolve();
          }
        });
      }
      else {
        _this._saveServer(interaction, group, host, alias, pinghost, parseInt(parts[1]));
        resolve();
      }
    });
  }

  _saveServer(interaction, group, host, alias, pinghost, pingport) {

    dns.resolve4(pinghost, (_err4, answer4) =>
      dns.resolve6(pinghost, (_err6, answer6) => {
        if (answer4 != undefined || answer6 != undefined) {
          const server = {
            host,
            alias,
            pinghost,
            pingport,
          };
          group.servers.push(server);
          interaction.reply(`Added server: ${server.alias} - \`${server.host}\`\nResolving to: ${pinghost}:${pingport}`);
          _this.save();
        }
        else {
          interaction.reply(`Could not resolve server dns: ${alias} - \`${host}\`\nResolving to: ${pinghost}:${pingport}`);
        }
      }),
    );
  }

  async _ampLogin(group, interaction) {
    const options = interaction.options;
    const ampserver = {
      hostname: options.getString('hostname', true),
      username: options.getString('username', true),
      password: options.getString('password', true),
      rememberMeToken: '',
    };
    group.ampserver = ampserver;
    _this.save();
    return await _this._doAmpLogin(ampserver).then((API) => {
      const tempMap = new Map();
      tempMap.set('main', API);
      ampCache.set(group.id, tempMap);
      return interaction.reply(`Logged into ${ampserver.hostname}`);
    }).catch((loginResult) => {
      console.log(loginResult);
      return interaction.reply(`Failed to login to ${ampserver.hostname}`);
    });
  }

  async _doAmpLogin(ampserver) {
    return new Promise((resolve, reject) => {
      const API = new ampapi.AMPAPI(ampserver.hostname);
      API.initAsync().then((APIInitOK) => {
        if (!APIInitOK) {
          console.log(`Failed to log into ${ampserver.hostname}`);
          reject(APIInitOK);
          return;
        }
        if (!ampserver.rememberMeToken) {
          ampserver.rememberMeToken = '';
        }
        API.Core.LoginAsync(ampserver.username, ampserver.password, ampserver.rememberMeToken, true).then((loginResult) => {
          if (loginResult.success) {
            console.log('Login successful');
            API.sessionId = loginResult.sessionID;
            ampserver.rememberMeToken = loginResult.rememberMeToken;
            ampserver.password = '';
            API.ampserver = ampserver;
            _this.save();

            // Perform second-stage API initialization, we only get the full API data once we're logged in.
            API.initAsync().then(async (APIInitOK2) => {
              if (!APIInitOK2) {
                console.log('API Stage 2 Init failed');
                reject(APIInitOK2);
              }
              resolve(API);
            });
          }
          else {
            reject(loginResult);
            console.log('Login failed');
            console.log(loginResult);
          }
        });
      });
    });
  }

  async _ampStartInstance(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    const name = interaction.options.getString('instancename', true);
    interaction.deferReply();
    return this._ampGetInstancesAsync(API).then(async (instances) => {
      const instance = instances.reduce((finalValue, server) => {
        const tempID = server.AvailableInstances.reduce((finalValue2, cur) => cur.InstanceName == name ? cur : finalValue2
          , null);
        return tempID ? tempID : finalValue;
      }, null);
      await API.ADSModule.StartInstanceAsync(instance.InstanceName);
      return await interaction.reply(`Starting instance ${instance.FriendlyName}`);
    }).catch(async (error) => {
      console.log(error);
      return await interaction.reply(`Something went wrong\n${error}`);
    });
  }

  async _ampStopInstance(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    const name = interaction.options.getString('instancename', true);
    interaction.deferReply();
    return this._ampGetInstancesAsync(API).then(async (instances) => {
      const instance = instances.reduce((finalValue, server) => {
        const tempID = server.AvailableInstances.reduce((finalValue2, cur) => cur.InstanceName == name ? cur : finalValue2
          , null);
        return tempID ? tempID : finalValue;
      }, null);
      await API.ADSModule.StopInstanceAsync(instance.InstanceName);
      return await interaction.reply(`Stopping instance ${instance.FriendlyName}`);
    }).catch(async (error) => {
      console.log(error);
      return await interaction.reply(`Something went wrong\n${error}`);
    });
  }

  async _setChannel(group, interaction) {
    group.channel = interaction.channelId;
    await _this.save();
    return await interaction.reply(`Status channel set to: ${interaction.channel.name}`);
  }

  async _deleteServer(group, interaction) {
    const alias = interaction.options.getString('alias');
    const serverIndex = group.servers.findIndex((s) => s.alias == alias || s.host == alias);
    if (serverIndex == -1) {
      return await interaction.reply(`Error: Server ${alias} not found`);
    }
    else {
      const server = group.servers[serverIndex];
      group.servers.splice(serverIndex, 1);
      _this.save();
      return await interaction.reply(`Removed server: ${server.alias} - ${server.host} (${server.pinghost}:${server.pingport})`);
    }
  }

  async _listServers(group, interaction) {
    let message = '';
    if (group.channel !== undefined) {
      message += `Status channel is ${m_client.channels.cache.get(group.channel)}\n`;
    }
    if (group.servers.length === 0) {
      message += 'No servers stored';
    }
    const finalMessage = group.servers.reduce((previous, server) => `${previous}${server.alias} - ${server.host} (${server.pinghost}:${server.pingport}\n`, message);
    return await interaction.reply(finalMessage);
  }

  async _ampRefreshLogin(API) {
    return _this._doAmpLogin(API.ampserver).then(
      newAPI => {
        API.sessionID = newAPI.sessionID;
        return API.initAsync();
      },
    );
  }

  async _ampGetInstancesAsync(API) {
    return API.ADSModule.GetInstancesAsync().then(
      async instances => {
        if (!(instances instanceof Array)) {
          console.log(instances);
          return this._ampRefreshLogin(API).then(
            () => API.ADSModule.GetInstancesAsync(),
          );
        }
        return instances;
      },
    );
  }

  async _ampGetInstances(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    return await this._ampGetInstancesAsync(API).then(async (instances) => {
      const instanceText = instances.map((server) =>{
        return `**__${server.FriendlyName}__**\n` + server.AvailableInstances.map((instance) => {
          return `**${instance.FriendlyName}** > ${instance.Running ? 'Running' : 'Stopped'}\n${instance.Module}: ${instance.InstanceName}\n`;
        }).join('\n');
      }).join('\n\n');
      return await interaction.reply(`${instanceText}`);
    }).catch(async (error) => {
      console.log(error);
      return await interaction.reply(`Something went wrong\n${error}`);
    });
  }

  async _ampInfo(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    const ampserver = group.ampserver;
    const name = interaction.options.getString('instancename');
    interaction.deferReply();
    return await this._ampGetInstancesAsync(API).then(async (instances) => {
      const instance = instances.reduce((finalValue, server) => {
        const tempID = server.AvailableInstances.reduce((innerFinalValue, cur) =>
          cur.FriendlyName == name || cur.InstanceName == name ? cur : innerFinalValue
        , null);
        return tempID ? tempID : finalValue;
      }, null);
      if (!ampCache.get(group.id).has(instance.InstanceID)) {
        ampCache.get(group.id).set(instance.InstanceID, await API.InstanceAPI(instance.InstanceID, ampserver.username));
      }
      const instanceAPI = ampCache.get(group.id).get(instance.InstanceID);
      const status = await instanceAPI.Core.GetStatusAsync();
      const statusMessage = Object.keys(status.Metrics).reduce((message, currentState) =>
        `${message}${currentState}: ${status.Metrics[currentState].RawValue}${status.Metrics[currentState].Units} / ${status.Metrics[currentState].MaxValue}${status.Metrics[currentState].Units}\n`
      , '');
      return await interaction.editReply(`**__${instance.FriendlyName}__**\nStatus: ${status.State == 0 ? 'Stopped' : 'Running'}\n${statusMessage}\n`);
    }).catch(async (error) => {
      console.log(error);
      return await interaction.editReply(`Something went wrong\n${error}`);
    });
  }


  async _ampStartGame(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    const ampserver = group.ampserver;
    const name = interaction.options.getString('instancename', true);
    interaction.deferReply();
    return this._ampGetInstancesAsync(API).then(async (instances) => {
      const instance = instances.reduce((finalValue, server) => {
        const tempID = server.AvailableInstances.reduce((finalValue2, cur) =>
          cur.FriendlyName == name || cur.InstanceName == name ? cur : finalValue2
        , null);
        return tempID ? tempID : finalValue;
      }, null);
      if (!instance) {
        return await interaction.editReply(`Error: instance not found ${name}`);
      }
      if (!ampCache.get(group.id).has(instance.InstanceID)) {
        ampCache.get(group.id).set(instance.InstanceID, await API.InstanceAPI(instance.InstanceID, ampserver.username));
      }
      const instanceAPI = ampCache.get(group.id).get(instance.InstanceID);
      await instanceAPI.Core.StartAsync();
      return await interaction.editReply(`Starting ${instance.FriendlyName}`);
    }).catch(async (error) => {
      console.log(error);
      return await interaction.editReply(`Something went wrong\n${error}`);
    });
  }

  async _ampStopGame(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    const ampserver = group.ampserver;
    const name = interaction.options.getString('instancename', true);
    interaction.deferReply();
    return this._ampGetInstancesAsync(API).then(async (instances) => {
      const instance = instances.reduce((finalValue, server) => {
        const tempID = server.AvailableInstances.reduce((finalValue2, cur) =>
          cur.FriendlyName == name || cur.InstanceName == name ? cur : finalValue2
        , null);
        return tempID ? tempID : finalValue;
      }, null);
      if (!instance) {
        return await interaction.editReply(`Error: instance not found ${name}`);
      }
      if (!ampCache.get(group.id).has(instance.InstanceID)) {
        ampCache.get(group.id).set(instance.InstanceID, await API.InstanceAPI(instance.InstanceID, ampserver.username));
      }
      const instanceAPI = ampCache.get(group.id).get(instance.InstanceID);
      await instanceAPI.Core.StopAsync();
      return await interaction.editReply(`Stopping ${instance.FriendlyName}`);
    }).catch(async (error) => {
      console.log(error);
      return await interaction.editReply(`Something went wrong\n${error}`);
    });
  }

  async _waitForTask(API, friendlyName, taskId, interaction, resolve, reject) {
    return API.Core.GetUpdatesAsync().then((result) => {
      let done = false;
      result.Messages.filter((value) => value.Message == 'removeTask' && value.Parameters == taskId)
        .forEach(async () => {
          await interaction.editReply(`Updating ${friendlyName} Complete`);
          done = true;
          resolve();
        });
      if (done) {
        return;
      }
      result.Messages.filter((value) => value.Message == 'refreshTask' && value.Parameters.Id == taskId)
        .forEach(async (update) => {
          await interaction.editReply(`Updating ${friendlyName} ${update.Parameters.ProgressPercent}%\n${update.Parameters.Speed}`);
        });
      setTimeout(() => _this._waitForTask(API, friendlyName, taskId, interaction, resolve, reject), 2000);
    }).catch(async (error) => {
      reject(error);
      return await interaction.editReply(`Error while waiting for updates ${error}.`);
    });
  }

  async _ampUpdateGame(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    const ampserver = group.ampserver;
    const name = interaction.options.getString('instancename', true);
    interaction.deferReply();
    return this._ampGetInstancesAsync(API).then(async (instances) => {
      const instance = instances.reduce((finalValue, server) => {
        const tempID = server.AvailableInstances.reduce((finalValue2, cur) =>
          cur.FriendlyName == name || cur.InstanceName == name ? cur : finalValue2
        , null);
        return tempID ? tempID : finalValue;
      }, null);
      if (!instance) {
        return await interaction.editReply(`Error: instance not found ${name}`);
      }
      if (!ampCache.get(group.id).has(instance.InstanceID)) {
        ampCache.get(group.id).set(instance.InstanceID, await API.InstanceAPI(instance.InstanceID, ampserver.username));
      }
      const instanceAPI = ampCache.get(group.id).get(instance.InstanceID);
      return instanceAPI.Core.UpdateApplicationAsync().then(async updateTask => {
        const taskId = updateTask.Id;
        return await interaction.editReply(`Updating ${instance.FriendlyName} ${updateTask.ProgressPercent}%`).then(() =>
          new Promise((resolve, reject) =>
            _this._waitForTask(instanceAPI, instance.FriendlyName, taskId, interaction, resolve, reject),
          ),
        );

      });
    }).catch(async (error) => {
      console.log(error);
      return await interaction.reply(`Something went wrong\n${error}`);
    });
  }

  async _ampWhitelist(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    const ampserver = group.ampserver;
    const name = interaction.options.getString('instancename', true);
    const user = interaction.options.getString('user', true);
    interaction.deferReply();
    return this._ampGetInstancesAsync(API).then(async (instances) => {
      const instance = instances.reduce((finalValue, server) => {
        const tempID = server.AvailableInstances.reduce((finalValue2, cur) =>
          cur.FriendlyName == name || cur.InstanceName == name ? cur : finalValue2
        , null);
        return tempID ? tempID : finalValue;
      }, null);
      if (!instance) {
        return await interaction.reply(`Error: instance not found ${name}`);
      }
      if (!ampCache.get(group.id).has(instance.InstanceID)) {
        ampCache.get(group.id).set(instance.InstanceID, await API.InstanceAPI(instance.InstanceID, ampserver.username));
      }
      const instanceAPI = ampCache.get(group.id).get(instance.InstanceID);
      await instanceAPI.MinecraftModule.AddToWhitelistAsync(user);
      return await interaction.editReply(`Whitelisting ${user} on ${instance.FriendlyName}`);
    }).catch(async (error) => {
      console.log(error);
      return await interaction.editReply(`Something went wrong\n${error}`);
    });
  }

  async _ampDeWhitelist(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    const ampserver = group.ampserver;
    const name = interaction.options.getString('instancename', true);
    const user = interaction.options.getString('user', true);
    interaction.deferReply();
    return this._ampGetInstancesAsync(API).then(async (instances) => {
      const instance = instances.reduce((finalValue, server) => {
        const tempID = server.AvailableInstances.reduce((finalValue2, cur) =>
          cur.FriendlyName == name || cur.InstanceName == name ? cur : finalValue2
        , null);
        return tempID ? tempID : finalValue;
      }, null);
      if (!instance) {
        return await interaction.editReply(`Error: instance not found ${name}`);
      }
      if (!ampCache.get(group.id).has(instance.InstanceID)) {
        ampCache.get(group.id).set(instance.InstanceID, await API.InstanceAPI(instance.InstanceID, ampserver.username));
      }
      const instanceAPI = ampCache.get(group.id).get(instance.InstanceID);
      return await instanceAPI.MinecraftModule.RemoveWhitelistEntryAsync(user)
        .then(async (response) => {
          console.log(response);
          return await interaction.editReply(`Dewhitelisting ${user} on ${instance.FriendlyName}`);
        }).catch(async (error) => {
          console.log(error);
          return await interaction.editReply(`Error while dewhitelisting ${user} on ${instance.FriendlyName}`);
        });
    }).catch(async (error) => {
      console.log(error);
      return await interaction.editReply(`Something went wrong\n${error}`);
    });
  }

  async _ampListWhitelist(group, interaction) {
    const API = ampCache.get(group.id).get('main');
    const ampserver = group.ampserver;
    const name = interaction.options.getString('instancename', true);
    interaction.deferReply();
    return this._ampGetInstancesAsync(API).then(async (instances) => {
      const instance = instances.reduce((finalValue, server) => {
        const tempID = server.AvailableInstances.reduce((finalValue2, cur) =>
          cur.FriendlyName == name || cur.InstanceName == name ? cur : finalValue2
        , null);
        return tempID ? tempID : finalValue;
      }, null);
      if (!instance) {
        return await interaction.editReply(`Error: instance not found ${name}`);
      }
      if (!ampCache.get(group.id).has(instance.InstanceID)) {
        ampCache.get(group.id).set(instance.InstanceID, await API.InstanceAPI(instance.InstanceID, ampserver.username));
      }
      const instanceAPI = ampCache.get(group.id).get(instance.InstanceID);
      const whitelist = await instanceAPI.MinecraftModule.GetWhitelistAsync();
      const whitelistString = whitelist.reduce((last, user) => `${last}${user.name}\n`, '');
      return await interaction.editReply(`__**Whitelist on ${instance.FriendlyName}**__\n${whitelistString}`);
    }).catch(async (error) => {
      console.log(error);
      return await interaction.editReply(`Something went wrong\n${error}`);
    });
  }

  async loadCache() {
    console.log('Loading Cache');
    await m_storage.getItem('groups')
      .then(async cachedGroups => cachedGroups === undefined || Promise.all(cachedGroups.map(async group => {
        return new Promise((resolve, reject) => {
          groups.push(group);
          if (group.ampserver) {
            return _this._doAmpLogin(group.ampserver).then(API => {
              const tempMap = new Map();
              tempMap.set('main', API);
              ampCache.set(group.id, tempMap);
              return resolve(group);
            }).catch(reject);
          }
          return resolve(group);
        });
      })));
  }

  getStatus(server) {
    return Gamedig.query({
      type: 'minecraft',
      host: server.pinghost,
      port: server.pingport,
    }).then((state) => {
      return { online: true, ...state, error: undefined };
    }).catch((error) => {
      return {
        online: false,
        players: [],
        error,
      };
    });
  }

  playerDifference(a, b) {
    return a.filter((p1) => b.findIndex(p2 => p2.id == p1.id) == -1);
  }

  updateStatus() {
    groups.forEach(group => {
      const channel = m_client.channels.cache.get(group.channel);
      Promise.all(group.servers.map(server =>
        _this.getStatus(server).then(status => ({
          server,
          status,
        })),
      )).then(results => {
        results.forEach(result => {
          const id = `${group.id} ${result.server.alias}`;
          if (!serverCache.has(id)) {
            serverCache.set(id, result);
            return;
          }
          const cache = serverCache.get(id);
          if (cache.status.online != result.status.online) {
            channel.send(`Server ${result.server.alias} is now ${result.status.online ? 'online' : 'offline'}`);
            serverCache.set(id, result);
            return;
          }
          if (result.status.online) {
            const offlinePlayers = _this.playerDifference(cache.status.players, result.status.players);
            const onlinePlayers = _this.playerDifference(result.status.players, cache.status.players);
            offlinePlayers.forEach(player => channel.send(`${player.name} left server ${result.server.alias}`));
            onlinePlayers.forEach(player => channel.send(`${player.name} joined server ${result.server.alias}`));
            if (onlinePlayers.length > 0 || offlinePlayers.length > 0) {
              serverCache.set(id, result);
            }
          }
        });
      });
    });
  }
}

module.exports = MinecraftBot;