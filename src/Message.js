export default class Message {
  constructor(channel, content) {
    this.channel = channel;
    this.content = content;
    this.author = {
      bot: false,
      id: 'test',
    };
    this.client = {
      user: {
        id: 'bot',
      },
    };
  }
}